package br.mg.prdioliveira.steps;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.mg.prdioliveira.pages.BaseObject;
import br.mg.prdioliveira.pages.TodoAppLoginPage;
import br.mg.prdioliveira.pages.TodoAppMainPage;
import br.mg.prdioliveira.pages.TodoAppModalSubtask;
import br.mg.prdioliveira.pages.TodoAppMyTasksPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class CriarSubTarefasSteps {
	
	private BaseObject baseObject = new BaseObject();
	private TodoAppLoginPage login = new TodoAppLoginPage(baseObject.getDriver());
	private TodoAppMainPage mainPage;
	private TodoAppMyTasksPage criarTarefa;
	private TodoAppModalSubtask criarSubtask;
	
	@Dado("que estou na tela Lista de Tarefas")
	public void queEstouNaTelaListaDeTarefas() {
		navegar();
		logar();
		acessarPaginaListaTarefas();
	    
	}

	@Dado("que tenha uma ou mais tarefas criadas")
	public void queTenhaUmaOuMaisTarefasCriadas() {
		criarTarefa.criarTaskComEnter("Task 1");
	}

	@Quando("visualizo a lista de tarefas vejo o botao {string}")
	public void visualizoAListaDeTarefasVejoOBotao(String txtBotao) {
		Assert.assertTrue(criarTarefa.getTxtBtnManagSubtask().contains(txtBotao));
	}

	@Quando("clico sobre o botao Manage Subtasks")
	public void clicoSobreOBotaoManageSubtasks() {
		criarSubtask = criarTarefa.managaeSubtaskClick();
	}

	@Entao("sera mostrado um modal para o cadastro da subtask")
	public void seraMostradoUmModalParaOCadastroDaSubtask(){
		String getTaskInSubtask = baseObject.getDriver().findElement(By.tagName("h4")).getText();
		Assert.assertEquals("Todo:", getTaskInSubtask);
	}

	@Dado("que nesse modal vejo o campo somente leitura com o id da tarefa")
	public void queNesseModalVejoOCampoSomenteLeituraComOIdDaTarefa() {
	    String taskId = baseObject.getDriver().findElement(By.tagName("h3")).getText();
	    Assert.assertTrue(taskId.contains("Editing Task"));
	}

	@Quando("preencho o campo SubTask Description com o valor {string}")
	public void preenchoOCampoSubTaskDescriptionComOValor(String description) {
	    criarSubtask.preencherSubTaskDescrtion(description);
	}

	@Quando("^preencho o campo Due date com o valor (.*)?$")
	public void preenchoOCampoDueDateComOValor(String data) {
	    criarSubtask.preencherSubTaskDueDate(data);
	}

	@Quando("seleciono o botao Adicionar")
	public void selecionoOBotaoAdicionar() {
	    criarSubtask.criarSubTarefa();
	}

	@Entao("^a subtarefa \"(.*)\" eh anexada na parte inferior do modal$")
	public void aSubtarefaEhAnexadaNaParteInferiorDoModal(String subtask) {
		WebDriverWait wait = new WebDriverWait(baseObject.getDriver(), 20);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='SubTask']")));
		
		String subtaskAnexada = baseObject.getDriver().findElement(By.xpath("//td[starts-with(@class,'task_body col-md-8 limit-word-')]")).getText();
		Assert.assertEquals(subtask, subtaskAnexada);
	}

	@Entao("quando aciono o botao close")
	public void quandoAcionoOBotaoClose() {
	    criarSubtask.closeModal();
	}

	@Entao("eh mostrada a tela Lista de Tarefas")
	public void ehMostradaATelaListaDeTarefas() {
	    Assert.assertEquals("Paulo's ToDo List", baseObject.getDriver().findElement(By.tagName("h1")).getText());
	}

	@Entao("o botao Manage Subtasks possui a quantidade de subtarefas igual a {int}")
	public void oBotaoManageSubtasksPossuiAQuantidadeDeSubtarefasIgualA(Integer qdtSubTask) {
		String txtBtnManage = baseObject.getDriver().findElement(By.xpath("//button[starts-with(@class,'btn btn-xs btn-primary')]")).getText();
		Assert.assertTrue(txtBtnManage.contains("(" + qdtSubTask + ")"));
	}
	
	
	public void navegar() {
		this.baseObject.navegar("https://qa-test.avenuecode.com/users/sign_in");
	}
	
	public void logar() {
		this.mainPage = login.logar("paulo@paulo.com", "paulo@123456");
	}
	
	public void acessarPaginaListaTarefas() {
		this.criarTarefa = mainPage.linkTaskClik();
	}
	
	@After
	public void fecharNavegador() {
		this.baseObject.fecharNavegadotr();
	}

}
