package br.mg.prdioliveira.pages;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TodoAppModalSubtask extends BaseObject{
	
	public String getTitle() {
		return getDriver().getTitle();
	}
	
	public TodoAppModalSubtask(WebDriver driver) {
		super(driver);
	}
 
	
	@FindBy(how = How.TAG_NAME, using = "h3")
	public WebElement txtTaskId;
	
	@FindBy(how = How.ID, using = "edit_task")
	public WebElement txtDescTaskEdit;
	
	@FindBy(how = How.ID, using = "new_sub_task")
	public WebElement txtSubTaskDescription;
	
	@FindBy(how = How.ID, using = "dueDate")
	public WebElement txtDueDate;
	
	@FindBy(how = How.ID, using = "add-subtask")
	public WebElement txtBtnAdd;
	
	@FindBy(how = How.ID, using = "//td[starts-with(@class,'task_body col-md-8 limit-word-')]")
	public WebElement subTaskItemList;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Close']")
	public WebElement txtBtnClose;
	
	public void criarSubTarefa() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.id("add-subtask")));
		
		element.click();
		System.out.println(element.getText());
	}
	
	public void preencherSubTaskDescrtion(String description) {
		txtSubTaskDescription.sendKeys(description);
	}
	
	public void preencherSubTaskDueDate(String data) {
		txtDueDate.clear();
		txtDueDate.sendKeys(data);
	}
	
	public void closeModal() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Close']")));
		
		element.click();
	}
	
	
	

}
