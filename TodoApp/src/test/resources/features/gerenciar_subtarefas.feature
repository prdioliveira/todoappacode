# language: pt

Funcionalidade: Criar subtarefas com sucesso
	Como usuario do ToDo App
	Eu deveria ser capaz de criar uma subtarefa
	Para que eu possa dividir minhas tarefas em peda�os menores
	
	
Contexto: Abrir modal de subtarefas
	Dado que estou logado com o usuario "Paulo"
	E que estou na tela Lista de Tarefas
	E que tenha uma ou mais tarefas criadas
	Quando visualizo a lista de tarefas vejo o botao "Manage Subtasks"
	E clico sobre o botao Manage Subtasks
	Entao sera mostrado um modal para o cadastro da subtask

Cenario: Criar subtarefa com sucesso	
	Dado que nesse modal vejo o campo somente leitura com o id da tarefa
	Quando preencho o campo SubTask Description com o valor "Subtask 1"
	E preencho o campo Due date com o valor 06/15/2020
	E seleciono o botao Adicionar
	Entao a subtarefa "Subtask 1" eh anexada na parte inferior do modal
	E quando aciono o botao close
	Entao eh mostrada a tela Lista de Tarefas
	E o botao Manage Subtasks possui a quantidade de subtarefas igual a 1
	
Esquema do Cenario: Data com valores invalidos

Cenario: Nao deve inserir subtarefa com data no formato invalido
	Dado que nesse modal vejo o campo somente leitura com o id da tarefa
	Entao preencho o campo SubTask Description com o valor "<subtask>"
	E preencho o campo Due date com o valor <data>
	E seleciono o botao Adicionar
	Entao a subtarefa "<subtask>" eh anexada na parte inferior do modal
	E quando aciono o botao close
	Entao eh mostrada a tela Lista de Tarefas
	E o botao Manage Subtasks possui a quantidade de subtarefas igual a 0
	
Exemplos:
	|  subtask    |     data     |
	|  Subtask 1  |  15/06/2020  |
	|  Subtask 2  |  2020/06/15  |
	
	
Esquema do Cenario: Campos obrigatorios nao preenchidos
	
Cenario: Nao deve inserir subtarefa sem preencher campos obrigatorios
	Dado que nesse modal vejo o campo somente leitura com o id da tarefa
	Entao preencho o campo SubTask Description com o valor "<description>"
	E preencho o campo Due date com o valor <data>
	E seleciono o botao Adicionar
	Entao a subtarefa "<description>" eh anexada na parte inferior do modal
	E quando aciono o botao close
	Entao eh mostrada a tela Lista de Tarefas
	E o botao Manage Subtasks possui a quantidade de subtarefas igual a 0
	
	Exemplos:
	|  description   |     data     |
	|                |  06/15/2020  |
	|  Subtask 2     |              |