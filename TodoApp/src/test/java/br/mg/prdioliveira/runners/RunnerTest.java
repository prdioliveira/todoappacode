package br.mg.prdioliveira.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/features/gerenciar_tarefas.feature",
							 "src/test/resources/features/gerenciar_subtarefas.feature"},
				 glue = "",
				 tags = "",
				 plugin = {"pretty", "html:target/report.html", "json:target/report.json"},
				 monochrome = true,
				 snippets = SnippetType.CAMELCASE,
				 dryRun = false)
public class RunnerTest {

}
