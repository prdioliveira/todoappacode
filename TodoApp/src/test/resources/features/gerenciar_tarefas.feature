# language: pt

Funcionalidade: Criar tarefa
	Como usu�rio do ToDo App
	Eu deveria ser capaz de criar uma tarefa
	Para que eu possa gerenciar minhas tarefas
	
Contexto: Abrir a tela de tarefas criadas
	Dado que estou logado com o usuario "Paulo"
	Quando acesso a pagina inicial vejo o link "My Tasks" e clico sobre ele
	Entao estou na tela Lista de Tarefas


Esquema do Cenario: Criacao de tasks

Cenario: Criar tarefa com sucesso usando a tecla enter
	Dado que estou na tela Lista de Tarefas e visualizo a mensagem "Hey Paulo, this is your todo list for today:"
	Quando preencho o campo da tarefa com "<task>" e pressiono a tecla enter
	Entao a nova tarefa "<task>" deve ser anexada a lista de tarefas criadas

Exemplos:
	|   task    |
  |  Task 1   |
  |  Task 2   |
  |  Tsk      |

	
Esquema do Cenario: Validacao de 3 caracteres

Cenario: Nao deve inserir tarefas com menos tres caracteres
	Dado que estou na tela Lista de Tarefas e visualizo a mensagem "Paulo's ToDo List"
	Quando preencho o campo da tarefa com "<tarefa>" e clico sobre o botao Adicionar
	Entao a nova tarefa "<tarefa>" Nao deve ser anexada a lista de tarefas criadas
	
Exemplos:
	| tarefa  |
  |  Ta     |
  |  T      |