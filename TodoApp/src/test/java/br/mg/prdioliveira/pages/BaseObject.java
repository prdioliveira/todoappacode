package br.mg.prdioliveira.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

public class BaseObject {
	protected WebDriver driver;
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public BaseObject (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public BaseObject() {
		System.setProperty("webdriver.gecko.driver", "src/test/resources/tools/geckodriver.exe");
		this.driver = new FirefoxDriver();
		this.driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
	
	public void navegar(String url) {
		driver.navigate().to(url);
	}
	
	public void fecharNavegadotr() {
		getDriver().close();
	}
	
}
