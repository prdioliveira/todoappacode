package br.mg.prdioliveira.steps;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import br.mg.prdioliveira.pages.BaseObject;
import br.mg.prdioliveira.pages.TodoAppMyTasksPage;
import br.mg.prdioliveira.pages.TodoAppLoginPage;
import br.mg.prdioliveira.pages.TodoAppMainPage;
import io.cucumber.java.After;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;


public class CriarTarefasSteps {
	
	private BaseObject baseObject = new BaseObject();
	private TodoAppLoginPage login = new TodoAppLoginPage(baseObject.getDriver());
	private TodoAppMainPage mainPage;
	private TodoAppMyTasksPage criarTarefa;
	
	@Dado("que estou logado com o usuario {string}")
	public void queEstouLogadoComOUsuario(String usuarioLogado) {
		navegar();
		logar();
		String userlogado = baseObject.getDriver().findElement(By.linkText("Welcome, Paulo!")).getText();
		Assert.assertTrue(userlogado.contains(usuarioLogado));

	}

	@Quando("acesso a pagina inicial vejo o link {string} e clico sobre ele")
	public void acessoAPaginaInicialVejoOLink(String myTask) {
		String linkText = baseObject.getDriver().findElement(By.id("my_task")).getText();
		Assert.assertEquals(myTask, linkText);
		this.criarTarefa = mainPage.linkTaskClik();
	}

	@Entao("estou na tela Lista de Tarefas")
	public void estouNaTelaListaDeTarefas() {
		baseObject.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String currentUrl = baseObject.getDriver().getCurrentUrl();
		Assert.assertEquals("https://qa-test.avenuecode.com/tasks", currentUrl);
	}

	@Dado("que estou na tela Lista de Tarefas e visualizo a mensagem {string}")
	public void queEstouNaTelaListaDeTarefasEVisualizoAMensagem(String msg) {
		String msgTodoList = baseObject.getDriver().findElement(By.tagName("h1")).getText();
		Assert.assertEquals(msg, msgTodoList);
	}

	@Quando("preencho o campo da tarefa com {string} e pressiono a tecla enter")
	public void preenchoOCampoDaTarefaCom(String task) {
		this.criarTarefa.criarTaskComEnter(task);
	}
	
	@Quando("^preencho o campo da tarefa com \"(.*)\" e clico sobre o botao Adicionar$")
	public void preenchoOCampoDaTarefaComEClicoSobreOBotaoAdicionar(String task) {
		this.criarTarefa.criarTaskComBotao(task);
	}

	@Entao("a nova tarefa {string} deve ser anexada a lista de tarefas criadas")
	public void aNovaTarefaDeveSerAnexadaAListaDeTarefasCriadas(String tarefa) {
		String taskInTable = baseObject.getDriver().findElement(By.xpath("//td[starts-with(@class,'task_body col-md-7 limit-word-')]")).getText();
		Assert.assertEquals(tarefa, taskInTable);
	}
	
	@Entao("a nova tarefa {string} Nao deve ser anexada a lista de tarefas criadas")
	public void aNovaTarefaNaoDeveSerAnexadaAListaDeTarefasCriadas(String tarefa) {
		String taskInTable = baseObject.getDriver().findElement(By.xpath("//td[starts-with(@class,'task_body col-md-7 limit-word-')]")).getText();
		Assert.assertFalse(tarefa.equals(taskInTable));
	}
	
	public void navegar() {
		this.baseObject.navegar("https://qa-test.avenuecode.com/users/sign_in");
	}
	
	public void logar() {
		this.mainPage = login.logar("paulo@paulo.com", "paulo@123456");
	}
	
	@After
	public void fecharNavegador() {
		this.baseObject.fecharNavegadotr();
	}
}