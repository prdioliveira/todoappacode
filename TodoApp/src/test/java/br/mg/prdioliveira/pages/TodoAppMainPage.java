package br.mg.prdioliveira.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TodoAppMainPage extends BaseObject {
	
	public TodoAppMainPage(WebDriver driver) {
		super(driver);
	}
	
	public String getTitle() {
		return getDriver().getTitle();
	}
	
	@FindBy(how = How.ID, using = "my_task")
	public WebElement txtLinkTask;
	
	@FindBy(how = How.LINK_TEXT, using = "Candidate Instructions")
	public WebElement txtMsgUserLogado;
	
	public TodoAppMyTasksPage linkTaskClik() {
		txtLinkTask.click();
		return new TodoAppMyTasksPage(getDriver());
	}
}
