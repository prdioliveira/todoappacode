package br.mg.prdioliveira.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TodoAppLoginPage extends BaseObject {
	
	public String getTitle() {
		return getDriver().getTitle();
	}
	
	public TodoAppLoginPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(how = How.ID, using = "user_email")
	public WebElement txtUsername;
	
	
	@FindBy(how = How.ID, using = "user_password")
	public WebElement txtPassword;
	
	@FindBy(how = How.NAME, using = "commit")
	public WebElement txtSignIn;
	
	public TodoAppMainPage logar(String username, String password) {
		fazerLogin(username, password);
		return new TodoAppMainPage(getDriver());
	}
	
	public void fazerLogin(String username, String password) {
		txtUsername.sendKeys(username);
		txtPassword.sendKeys(password);
		txtSignIn.click();
	}
}
