package br.mg.prdioliveira.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TodoAppMyTasksPage extends BaseObject {

	public TodoAppMyTasksPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public String getTitle() {
		return getDriver().getTitle();
	}
	
	WebDriverWait wait = new WebDriverWait(getDriver(), 20);
	WebElement element = wait.until(
	        ExpectedConditions.visibilityOfElementLocated(By.xpath("//th[text()='SubTasks']")));
	
	@FindBy(how = How.ID, using = "new_task")
	public WebElement txtFieldNewTask;
	
	@FindBy(how = How.XPATH, using = "//span[starts-with(@class,'input-group-addon glyphicon glyphicon-')]")
	public WebElement txtBtnAdd;
	
	@FindBy(how = How.XPATH, using = "//button[starts-with(@class,'btn btn-xs btn-primary')]")
	public WebElement txtBtnManageSubtask;
	
	public void criarTaskComEnter(String task) {
		txtFieldNewTask.sendKeys(task);
		txtFieldNewTask.sendKeys(Keys.ENTER);
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[starts-with(@class,'task_body col-md-7 limit-word-')]")));
	}
	
	public void criarTaskComBotao(String task) {
		txtFieldNewTask.sendKeys(task);
		txtBtnAdd.click();
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		WebElement element = wait.until(
		        ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[starts-with(@class,'task_body col-md-7 limit-word-')]")));
	}
	
	public TodoAppModalSubtask managaeSubtaskClick() {
		txtBtnManageSubtask.click();
		return new TodoAppModalSubtask(getDriver());
	}
	
	public String getTxtBtnManagSubtask() {
		return txtBtnManageSubtask.getText();
	}

}
